-- 商品表
CREATE TABLE products (
  pid NUMBER(6) NOT NULL ,  
  pname VARCHAR2(40),
  price NUMBER(8,2),  
  stock NUMBER(4),
  CONSTRAINT products_pk PRIMARY KEY (pid)
    USING INDEX TABLESPACE userindex  
) 
TABLESPACE userdata;

-- 客户表 
CREATE TABLE customers (
  cid NUMBER(6) NOT NULL ,  
  cname VARCHAR2(40),
  address VARCHAR2(200),
  CONSTRAINT customers_pk PRIMARY KEY (cid)  
)
TABLESPACE userdata;

-- 订单表
CREATE TABLE orders (
  oid NUMBER(6) NOT NULL ,      
  cid NUMBER(6) NOT NULL , 
  odate DATE,
  CONSTRAINT orders_pk PRIMARY KEY (oid)  ,
  CONSTRAINT fk_customers 
    FOREIGN KEY (cid) REFERENCES customers(cid)
)
TABLESPACE userdata;

-- 订单详情表
CREATE TABLE order_items (
  oid NUMBER(6) NOT NULL ,  
  pid NUMBER(6) NOT NULL , 
  num NUMBER(4) ,
  price NUMBER(8,2), 
  CONSTRAINT fk_orders 
    FOREIGN KEY (oid) REFERENCES orders(oid),
  CONSTRAINT fk_products
    FOREIGN KEY (pid) REFERENCES products(pid)
)  
TABLESPACE userdata; 