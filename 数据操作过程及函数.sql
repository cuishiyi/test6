CREATE OR REPLACE PACKAGE PACK_SALE AS
  -- 存储过程：计算订单金额
  PROCEDURE calculate_order_amount(p_order_id IN NUMBER);

  -- 存储过程：修改商品价格
  PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER);

  -- 存储过程：生成订单编号
  PROCEDURE generate_order_id(p_customer_id IN NUMBER);

  -- 函数：计算每个客户的订单总金额
  FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER;

  -- 函数：计算每个商品的销售总额
  FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER;
END PACK_SALE;
/

CREATE OR REPLACE PACKAGE BODY PACK_SALE AS
  -- 存储过程：计算订单金额
  PROCEDURE calculate_order_amount(p_order_id IN NUMBER) AS
    v_order_amount NUMBER;
  BEGIN
    -- 在这里编写计算订单金额的逻辑，将结果存储到 v_order_amount 变量中

    -- 示例逻辑：假设订单金额为订单详情表中数量（quantity）乘以单价（price）的总和
    SELECT SUM(quantity * price) INTO v_order_amount
    FROM order_items
    WHERE order_id = p_order_id;

    -- 输出订单金额
    DBMS_OUTPUT.PUT_LINE('Order Amount: ' || v_order_amount);
  END calculate_order_amount;

  -- 存储过程：修改商品价格
  PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER) AS
  BEGIN
    -- 在这里编写修改商品价格的逻辑

    -- 示例逻辑：更新商品表中的价格字段
    UPDATE products
    SET price = p_new_price
    WHERE product_id = p_product_id;
  END update_product_price;

  -- 存储过程：生成订单编号
  PROCEDURE generate_order_id(p_customer_id IN NUMBER) AS
    v_order_id NUMBER;
  BEGIN
    -- 在这里编写生成订单编号的逻辑，将结果存储到 v_order_id 变量中

    -- 示例逻辑：假设订单编号为自增长序列的下一个值
    SELECT order_id_seq.NEXTVAL INTO v_order_id
    FROM dual;

    -- 插入新的订单记录
    INSERT INTO orders(order_id, customer_id, order_date)
    VALUES (v_order_id, p_customer_id, SYSDATE);

    -- 输出生成的订单编号
    DBMS_OUTPUT.PUT_LINE('Generated Order ID: ' || v_order_id);
  END generate_order_id;

  -- 函数：计算每个客户的订单总金额
  FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER AS
    v_total_amount NUMBER;
  BEGIN
    -- 在这里编写计算每个客户的订单总金额的逻辑，将结果存储到 v_total_amount 变量中

    -- 示例逻辑：计算订单详情表中某客户的订单金额总和
    SELECT SUM(quantity * price) INTO v_total_amount
    FROM order_items oi
    INNER JOIN orders o ON oi.order_id = o.order_id
    WHERE o.customer_id = p_customer_id;

    -- 返回订单总金额
    RETURN v_total_amount;
  END calculate_total_order_amount;

  -- 函数：计算每个商品的销售总额
  FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER AS
    v_total_amount NUMBER;
  BEGIN
    -- 在这里编写计算每个商品的销售总额的逻辑，将结果存储到 v_total_amount 变量中

    -- 示例逻辑：计算订单详情表中某商品的销售总额
    SELECT SUM(quantity * price) INTO v_total_amount
    FROM order_items
    WHERE product_id = p_product_id;

    -- 返回销售总额
    RETURN v_total_amount;
  END calculate_total_sales_amount;
END PACK_SALE;
/
