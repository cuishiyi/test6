-- 创建用户数据表空间 
CREATE TABLESPACE userdata 
DATAFILE 'userdata.dbf' 
SIZE 100M AUTOEXTEND ON;

-- 创建用户索引表空间
CREATE TABLESPACE userindex 
DATAFILE 'userindex.dbf' 
SIZE 50M AUTOEXTEND ON;


